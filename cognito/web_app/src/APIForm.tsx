import React from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import Radio from "@material-ui/core/Radio";
import Amplify, { API } from "aws-amplify";

type Props = {
  endpoint?: string;
  path?: string;
  setResponseText: React.Dispatch<React.SetStateAction<string>>;
};

function StyledRadio() {
  return <Radio disableRipple color="default" checkedIcon={<span />} />;
}

const APIForm = (props: Props) => {
  const { endpoint, path, setResponseText } = props;
  return (
    <Formik
      initialValues={{ data: "", endpoint, path, requestMethod: "GET" }}
      onSubmit={values => {
        console.log(JSON.stringify(values));
        Amplify.configure({
          API: {
            endpoints: [
              {
                name: "test-api",
                endpoint: values.endpoint
              }
            ]
          }
        });
        console.log("Amplify reconfigured");
        if (values.endpoint) {
          switch (values.requestMethod) {
            case "GET":
              API.get("test-api", values.path, {})
                .then(response => {
                  setResponseText(JSON.stringify(response.data, null, 2));
                })
                .catch(error => {
                  setResponseText(JSON.stringify(error, null, 2));
                });
              break;
            case "POST":
              API.post("test-api", values.path, { response: true })
                .then(response => {
                  setResponseText(JSON.stringify(response.data, null, 2));
                })
                .catch(error => {
                  setResponseText(JSON.stringify(error, null, 2));
                });
              break;
            default:
              break;
          }
        }
      }}
    >
      {({ values }) => (
        <>
          <br />
          <Form>
            <label htmlFor="endpoint">API endpoint: </label>
            <Field type="endpoint" name="endpoint" />
            <ErrorMessage name="endpoint" component="div" />
            <br />
            <label htmlFor="path">API path: </label>
            <Field type="path" name="path" />
            <ErrorMessage name="path" component="div" />
            <br />
            <label htmlFor="requestMethod">Request Method: </label>
            <Field as="select" name="requestMethod">
              <option value="GET">GET</option>
              <option value="POST">POST</option>
            </Field>
            {values.requestMethod === "POST" && (
              <Field as="textarea" name="data"></Field>
            )}
            <br />
            <button type="submit">{values.requestMethod}</button>
          </Form>
        </>
      )}
    </Formik>
  );
};

export default APIForm;
