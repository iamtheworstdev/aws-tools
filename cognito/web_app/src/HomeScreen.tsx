import React, { useState } from "react";
import { withAuthenticator, SignOut } from "aws-amplify-react";
import APIForm from "./APIForm";

type Props = {
  apiEndpoint?: string;
  apiPath?: string;
};

const signUpConfig = {
  header: "My Customized Sign Up",
  hideAllDefaults: true,
  defaultCountryCode: "1",
  signUpFields: [
    {
      label: "Email",
      key: "email",
      required: true,
      displayOrder: 4,
      type: "string"
    },
    {
      label: "Password",
      key: "password",
      required: true,
      displayOrder: 2,
      type: "password"
    }
  ]
};
const usernameAttributes = "My user name";

const HomeScreen = (props: Props) => {
  const [responseText, setResponseText] = useState("");
  const { apiEndpoint, apiPath } = props;
  return (
    <div>
      <APIForm
        endpoint={apiEndpoint}
        path={apiPath}
        setResponseText={setResponseText}
      />
      <br />
      <SignOut />
      <br />
      <pre className="api-response">{responseText}</pre>
    </div>
  );
};

export default withAuthenticator(HomeScreen, undefined, undefined, undefined, {
  usernameAttributes: "email"
});
