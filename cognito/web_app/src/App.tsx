import React, { useState } from "react";
import "./App.css";
import HomeScreen from "./HomeScreen";
import CognitoForm from "./CognitoForm";
import Amplify, { Auth } from "aws-amplify";

export type CognitoAttrs = {
  identityPoolId: string;
  identityPoolRegion: string;
  region: string;
  userPoolId?: string;
  userPoolWebClientId?: string;
};

const App: React.FC = () => {
  const [cognitoAttrs, setCognitoAttrs] = useState({
    identityPoolId: process.env.REACT_APP_IDENTITY_POOL_ID || "",
    identityPoolRegion: process.env.REACT_APP_IDENTITY_POOL_REGION || "",
    region: process.env.REACT_APP_REGION || "",
    userPoolId: process.env.REACT_APP_USER_POOL_ID || "",
    userPoolWebClientId: process.env.REACT_APP_USER_POOL_WEB_CLIENT_ID || ""
  });
  const [apiEndpoint, setApiEndpoint] = useState(
    process.env.REACT_APP_API_ENDPOINT || ""
  );
  const [apiPath, setApiPath] = useState(process.env.REACT_APP_API_PATH || "");
  const [requestMethod, setRequestMethod] = useState(
    process.env.REACT_APP_REQUEST_METHOD || ""
  );

  const setAuth = async (
    identityPoolId: string,
    identityPoolRegion: string,
    region: string,
    userPoolId?: string,
    userPoolWebClientId?: string,
    updateState?: boolean
  ) => {
    Amplify.configure({
      Auth: {
        identityPoolId,
        identityPoolRegion,
        region,
        userPoolId,
        userPoolWebClientId
      }
    });
    if (updateState) {
      setCognitoAttrs({
        identityPoolId,
        identityPoolRegion,
        region,
        userPoolId: userPoolId || "",
        userPoolWebClientId: userPoolWebClientId || ""
      });
    }
    console.log("Amplify Auth configuration updated");
    console.log(JSON.stringify(await Auth.currentCredentials()));
  };

  if (cognitoAttrs.userPoolId) {
    setAuth(
      cognitoAttrs.identityPoolId,
      cognitoAttrs.identityPoolRegion,
      cognitoAttrs.region,
      cognitoAttrs.userPoolId,
      cognitoAttrs.userPoolWebClientId,
      false
    );
  }

  return (
    <div className="App">
      <CognitoForm onSubmit={setAuth} {...cognitoAttrs} />
      <HomeScreen
        apiEndpoint={apiEndpoint}
        apiPath={apiPath}
        requestMethod={requestMethod}
      />
    </div>
  );
};

export default App;
