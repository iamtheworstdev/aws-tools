import React from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import { CognitoAttrs } from "./App";

type Props = CognitoAttrs & {
  onSubmit: (
    identityPoolId: string,
    identityPoolRegion: string,
    region: string,
    userPoolId?: string,
    userPoolWebClientId?: string,
    updateState?: boolean
  ) => void;
};

const CognitoForm = (props: Props) => {
  const { onSubmit } = props;
  return (
    <Formik
      initialValues={{ ...props }}
      onSubmit={values => {
        console.log(`form values - ${JSON.stringify(values)}`);
        onSubmit(
          values.identityPoolId,
          values.identityPoolRegion,
          values.region,
          values.userPoolId,
          values.userPoolWebClientId,
          true
        );
      }}
    >
      {() => (
        <>
          <br />
          <Form>
            <label htmlFor="identityPoolId">Identity Pool Id: </label>
            <Field type="identityPoolId" name="identityPoolId" />
            <ErrorMessage name="identityPoolId" component="div" />
            <br />
            <label htmlFor="identityPoolRegion">Identity Pool Region: </label>
            <Field type="identityPoolRegion" name="identityPoolRegion" />
            <ErrorMessage name="identityPoolRegion" component="div" />
            <br />
            <label htmlFor="region">Region: </label>
            <Field type="region" name="region" />
            <ErrorMessage name="region" component="div" />
            <br />
            <label htmlFor="userPoolId">User Pool Id: </label>
            <Field type="userPoolId" name="userPoolId" />
            <ErrorMessage name="userPoolId" component="div" />
            <br />
            <label htmlFor="userPoolWebClientId">
              User Pool Web Client Id:{" "}
            </label>
            <Field type="userPoolWebClientId" name="userPoolWebClientId" />
            <ErrorMessage name="userPoolWebClientId" component="div" />
            <br />
            <button type="submit">Set Cognito Options</button>
          </Form>
        </>
      )}
    </Formik>
  );
};

export default CognitoForm;
