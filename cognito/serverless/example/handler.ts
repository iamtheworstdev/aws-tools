import { APIGatewayProxyHandler } from "aws-lambda";
import "source-map-support/register";

export const hello: APIGatewayProxyHandler = async (event, _context) => {
  console.log(JSON.stringify(event));
  return {
    statusCode: 200,
    headers: {
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Credentials": true
    },
    body: JSON.stringify({
      data: "Hello!"
    })
  };
};
