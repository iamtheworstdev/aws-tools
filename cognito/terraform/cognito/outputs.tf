output "identity_pool_id" {
  value = aws_cognito_identity_pool.pool.id
}

output "identity_pool_arn" {
  value = aws_cognito_identity_pool.pool.arn
}

output "user_pool_arn" {
  value = module.cognito.arn
}

output "user_pool_creation_date" {
  value = module.cognito.creation_date
}

output "user_pool_endpoint" {
  value = module.cognito.endpoint
}

output "user_pool_id" {
  value = module.cognito.id
}

output "user_pool_last_modified_date" {
  value = module.cognito.last_modified_date
}

output "user_pool_web_client_id" {
  value = aws_cognito_user_pool_client.client.id
}

