provider "aws" {
  region = var.region
}

module "cognito" {
  source = "git::https://gitlab.com/iamtheworstdev/terraform_modules.git//v0.12.x/aws/cognito_user_pool_basic"
  providers = {
    aws = aws
  }
}

resource "aws_cognito_user_pool_client" "client" {
  name = "webclient"

  user_pool_id = module.cognito.id
}

resource "aws_cognito_identity_pool" "pool" {
  identity_pool_name               = "user_pool"
  allow_unauthenticated_identities = false

  cognito_identity_providers {
    client_id               = aws_cognito_user_pool_client.client.id
    provider_name           = "cognito-idp.${var.region}.amazonaws.com/${module.cognito.id}"
    server_side_token_check = false
  }
}

resource "aws_cognito_identity_pool_roles_attachment" "customer" {
  identity_pool_id = "${aws_cognito_identity_pool.pool.id}"

  roles = {
    "authenticated" = "${aws_iam_role.authenticated_user.arn}"
  }
}

resource "aws_iam_role" "authenticated_user" {
  name = "authenticated_user"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Federated": "cognito-identity.amazonaws.com"
      },
      "Action": "sts:AssumeRoleWithWebIdentity",
      "Condition": {
        "StringEquals": {
          "cognito-identity.amazonaws.com:aud": "${aws_cognito_identity_pool.pool.id}"
        },
        "ForAnyValue:StringLike": {
          "cognito-identity.amazonaws.com:amr": "authenticated"
        }
      }
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "authenticated_user" {
  name = "authenticated_user_policy"
  role = "${aws_iam_role.authenticated_user.id}"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": 
    [
        {
            "Effect": "Allow",
            "Action": [
                "cognito-identity:*",
                "mobileanalytics:PutEvents",
                "cognito-sync:*"
            ],
            "Resource": "*"
        },
        {
          "Effect": "Allow",
          "Action": [
            "execute-api:Invoke"
          ],
          "Resource": [
            "arn:aws:execute-api:us-east-1:*:*/*/*",
            "arn:aws:execute-api:us-west-2:*:*/*/*"
          ]
        }
    ]
}
EOF
}
