variable "domain" {
  type = "string"
}

variable "name" {
  type = "string"
}

variable "region" {
  type    = "string"
  default = "us-east-1"
}

variable "route53_zone_id" {
  type = "string"
}
