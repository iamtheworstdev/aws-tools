resource "aws_route53_zone" "api" {
  name = "${var.name}.${var.domain}"
}

resource "aws_route53_record" "api_subdomain" {
  name    = "${var.name}.${var.domain}"
  type    = "NS"
  zone_id = var.route53_zone_id
  ttl     = 300

  records = [
    aws_route53_zone.api.name_servers.0,
    aws_route53_zone.api.name_servers.1,
    aws_route53_zone.api.name_servers.2,
    aws_route53_zone.api.name_servers.3
  ]
}
