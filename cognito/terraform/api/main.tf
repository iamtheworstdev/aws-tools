provider "aws" {
  region = "${var.region}"
}

provider "aws" {
  alias  = "east"
  region = "us-east-1"
}

provider "aws" {
  alias  = "west"
  region = "us-west-2"
}

module "api" {
  source = "git::https://gitlab.com/iamtheworstdev/terraform_modules.git//v0.12.x/aws/multi_region_api"
  providers = {
    aws.east = "aws.east"
    aws.west = "aws.west"
  }

  domain           = "${var.name}.${var.domain}"
  name             = var.name
  route_53_zone_id = aws_route53_zone.api.zone_id
}
